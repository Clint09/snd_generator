      program main
! written by CMCC, last modified 5/3/2006
      implicit double precision (a-h,o-z)
      integer*8 npoints,k,naccepted,iwriteout
      parameter (mph=40)
      real*4 csi(1)
      dimension sump(0:mph-1),sum2p(0:mph-1)
      dimension fractions(0:mph-1)
      dimension xsecp(0:mph-1),varp(0:mph-1)
      dimension pin(0:3),p1(0:3),p2(0:3),qph(mph,0:3),q(0:3),qcut(0:3)
      dimension qphcut(mph,0:3)
      dimension pin1(0:3),pin2(0:3),ptmp(0:3),pbeam1(0:3),pbeam2(0:3)
      dimension pbeam1p(0:3),pbeam2p(0:3)
      dimension p1b(0:3),p2b(0:3)
      dimension dir(3)
      dimension p1cut(0:3),p2cut(0:3),pcm(0:3)
      integer isvec(25)
      character*6   ord
      character*10  model
      character*100 outfile,storefile
      character*3   eventlimiter,store
      common/beforesort/p1o(0:3),p2o(0:3),qpho(40,0:3)
      common/testsdebug/elmat2cmn,e2approx,s12,s34,t24,t13,u14,u23
      common/debug2/p1mod,odv
      common/eg_branchsampling/ibranch
*** filled in the subroutine userinterface
      character*2 fs
      common/finalstate/fs
      common/ecms/ecms
      common/nphot_mode/nphotmode
      common/expcuts/thmin,thmax,emin,zmax,egmin,thgmin,thgmax
      common/zparameters/zm,gz,ve,ae,rv,ra,wm,s2th,gfermi,sqrt2,um
      common/epssoft/eps
      common/energiabeam/ebeam
      common/parameters/ame,ammu,convfac,alpha,pi
      common/intinput/iwriteout,iseed,nsearch,iverbose
      common/qedORDER/ord
      common/charinput/model,eventlimiter,store,storefile,outfile
      common/realinput/anpoints,sdifmax
      common/nmaxphalpha/nphmaxalpha
      common/ialpharunning/iarun
      common/idebugging/idebug
      common/regolatori/regulator1,regulator2
*********************************************
      common/ggreg/ipair
      common/coseni/cmax,cmin
      common/channelref/iref
      common/momentainitial/pin1,pin2
      common/radpattern/nph(4)
      common/forborncrosssection/phsp2b,flux2b,bornme,bornmeq2,bornmez
      common/reducedtoborn/p1b,p2b
      common/samplelimits/spmin,omaxext
      common/various/beta,betafs
      common/for_debug/ppp(0:3),denphsp,dir3(0:3),dir4(0:3),br,bq,onm
      common/funvalues/funa,funb,func,funm
      common/enableifirst/ifrstenbld

      common/idarkon/idarkon
      common/darkmatter/amassU,gammaU,gvectU,gaxU

      call userinterface

** [[begin initialization]]
      if (idarkon.eq.1) then
        if (gammaU.lt.0.) then
          gammaU=gammafunc(amassU,gvectU)
        endif
        if (amassU.eq.0.) then
          amassU=1.d-13
        endif
        gvectU=gvectU*sqrt(4.*pi*alpha)
        gaxU=gaxU*sqrt(4.*pi*alpha)
        call init_apar
      endif

      idebug = 0
      cmax   = dcos(thmin)
      cmin   = dcos(thmax)
      npoints = anpoints

* for ALPHA, not released
*      call init_apar
      am1 = ame
      am2 = ame
      if (fs.eq.'gg') then
         am1 = 0.d0
         am2 = 0.d0
      endif
      if (fs.eq.'mm') then
         am1 = ammu
         am2 = ammu
      endif
      in_conf_spin = 4

      sum = 0.d0
      sum2 = 0.d0
      do k = 0,mph-1
         sump(k) = 0.d0
         sum2p(k) = 0.d0
      enddo

      nphmax       = 0
      xsec         = 0.d0
      var          = 0.1d0
      naccepted    = 0
      sumb1        = 0.d0
      sumb2        = 0.d0
      bornmax      = 0.d0
      phspmax      = 0.d0
      nwhenmax     = 0
      wmax         = 0.d0
      prodmax      = 0.d0
      elmmax       = 0.d0
      nover        = 0
      hitpmiss     = 0.d0
      hit          = 0.d0
      istopsearch  = 0
      nneg         = 0
      sumover      = 0.d0
      sum2over     = 0.d0
      sumneg       = 0.d0
      sum2neg      = 0.d0

      if (store.eq.'yes') call initstorage(storefile)
      k = 0
      wnpoints = npoints
      do j = 0,3
         do ki = 1,mph
            qph(ki,j) = 0.d0
         enddo
      enddo
      ng = 0

      do while(k.lt.wnpoints)
**** THE FOLLOWING LINES BETWEEN "*[[ start" AND "* end ]]" MUST
**** BE RECALCULATED FOR EACH EVENT IF ECMS CHANGES EVENT BY EVENT.
**** FURTHERMORE, IF ECMS CHANGES EVENT BY EVENT ifrstenbld MUST 
**** BE SET TO 0
*[[ start
        ifrstenbld = 0
        esoft = eps * ecms/2.d0
        pin(0) = ecms
        pin(2) = 0.d0
        pin(3) = 0.d0
        pin(1) = 0.d0
        betafs = sqrt(1.d0 - 4.d0*am1**2/pin(0)**2)
        beta   = sqrt(1.d0 - 4.d0*ame**2/pin(0)**2)
        pin1(0) = pin(0)/2.d0
        pin1(1) = 0.d0
        pin1(2) = 0.d0
        pin1(3) = beta * pin1(0)
        pin2(0) = pin(0)/2.d0
        pin2(1) = 0.d0
        pin2(2) = 0.d0
        pin2(3) = -beta * pin2(0)
        do ki = 0,3
           pbeam1(ki) = pin1(ki)
           pbeam2(ki) = pin2(ki)
           ptmp(ki)   = pin1(ki)+pin2(ki)
        enddo
        s = dot(ptmp,ptmp)
* end ]]



** [[end initialization]]

** for debugging!!
c      open(16,file='ranluxsequence',status='unknown')
c      read(16,*)isvec
c      call rluxin(isvec)
c      close(16)
***
         k = k + 1
** for debugging!!
c         if (k.eq.4708386) then
c            idebug = 1
c         else
c            idebug = 0
c         endif
c         if (k.eq.586) then
c            print*,'DUMPING RANLUX SEQUENCE'
c            open(16,file='ranluxsequence',status='unknown')
c            call rluxut(isvec)
c            write(16,*)isvec
c            close(16)
c         endif
c         print*,k
***
         flux = 8.d0 * (ecms/2.d0)**2
         if (fs.eq.'ee'.or.fs.eq.'mm') call get_cos_fer(cth,wcos)

         call multiplicity(eps,ecms,cth,ng,wnphot)

         if (fs.eq.'gg') then
            if (ng.eq.0) then 
               call get_cos_2g_born(cth,wcos)
            else
               call get_cos_2g(cth,wcos)
            endif
         endif
         sdif = wnphot*wcos
***************************************************
** initial state radiation with structure functions
*
      if (ord.eq.'struct') then
          call emission(pbeam1,pbeam2,wisr,'e')

          betafs = sqrt(1.d0 - 4d0*am1**2/(pbeam1(0)+pbeam2(0))**2)
          beta   = sqrt(1.d0 - 4d0*ame**2/(pbeam1(0)+pbeam2(0))**2)
*
          sreduced =   (pbeam1(0)+pbeam2(0))**2 
     +               - (pbeam1(3)+pbeam2(3))**2

          pbeam1p(0) = sqrt(sreduced)/2.d0
          pbeam1p(1) = 0d0
          pbeam1p(2) = 0d0
          pbeam1p(3) = beta*pbeam1p(0)
*
          pbeam2p(0) = pbeam1p(0)
          pbeam2p(1) = 0d0
          pbeam2p(2) = 0d0
          pbeam2p(3) = -pbeam1p(3)
*
          sdif = sdif * wisr
      else
          do kk=0,3
              pbeam1p(kk) = pbeam1(kk)
              pbeam2p(kk) = pbeam2(kk)
          enddo
      endif

*
** end of initial state radiation
*******************************************

         call phasespace(pbeam1p,pbeam2p,p1,p2,qph,ng,am1,am2,
     .        esoft,cth,w,phsp,ie)
         sdif = sdif * phsp * w

*
** final state radiation with structure function
*
          if (ord.eq.'struct'.and.ie.eq.0) then
              do kk = 0,3
                  p1cut(kk) = p1(kk)
                  p2cut(kk) = p2(kk)
                  pcm(kk)   = pbeam1(kk) + pbeam2(kk)
              enddo
              do ii = 1,ng
                  do kk = 0,3
                      qcut(kk)  = qph(ii,kk)
                  enddo
                  call new_boost(pcm,qcut,qcut,-1)
                  do kk = 0,3
                      qphcut(ii,kk)  = qcut(kk)
                  enddo
               enddo
                  
              call new_boost(pcm,p1cut,p1cut,-1)
              call new_boost(pcm,p2cut,p2cut,-1)
     
              if (fs.eq.'mm') call emission(p1cut,p2cut,wfsr,'m')
              if (fs.eq.'ee') call emission(p1cut,p2cut,wfsr,'e')
     
          else
              do kk = 0,3
                  p1cut(kk) = p1(kk)
                  p2cut(kk) = p2(kk)
              enddo
              do ii = 1,ng
                  do kk = 0,3
                      qphcut(ii,kk)  = qph(ii,kk)
                  enddo
              enddo
          endif
*
** end of final state radiation
*****************************************

         if (ie.ge.1) ie = 1
         if (fs.eq.'gg'.and.ie.eq.0) then
            call mixandsortmomenta(ng,p1,p2,qph)
         endif

         if (ie.lt.1) then 
             if (fs.eq.'ee'.or.fs.eq.'mm') then
                 call cuts(p1cut,p2cut,qphcut,icut)
             elseif (fs.eq.'gg') then
                 call cutsgg(ng,p1cut,p2cut,qphcut,icut)
             endif
         else
            icut = 1
         endif
         ie = ie + icut

         if (ng.gt.nphmax.and.ie.eq.0) nphmax = ng
         if (icut.eq.0) naccepted = naccepted + 1

!!         ie = 1   ! uncomment  for phase space integral

         call squared_matrix(model,ng,ecms,p1,p2,pbeam1,pbeam2,qph,
     >         ie,icalc,emtx,prod)

         bck  = emtx
         emtx = emtx/in_conf_spin ! divided by initial spin conf. 
         emtx = emtx*convfac/flux ! divided by the flux

         if (fs.eq.'gg') emtx = emtx/(ng+2)/(ng+1)

         if (ie.eq.0) then
            call svfactor(model,ng,ecms,p1,p2,eps,sv,deltasv)
            sdif = sdif * sv
         else
            sdif = 0.d0
         endif

         sdif = sdif * emtx

         if (sdif.lt.0.d0) then
             sdif = 0.d0
             w    = 0.d0
             ie   = 1
         endif

         if (ord.eq.'struct'.and.ie.eq.0) then
            do kk = 0,3
                p1(kk) = p1cut(kk)
                p2(kk) = p2cut(kk)
                qph(1,kk) = qcut(kk)
            enddo
            sdif = sdif * wfsr
         endif

! rescaling for Z exchange (bornmez = 0 for gg...)
         sdif = sdif/bornme*(bornme + bornmez)
!!         emtx = 1.d0 ! uncomment  for phase space integral
         iii = 0
         if (sdif.gt.sdifmax) then 
            sdifmax = sdif
            nwhenmax = ng
            iii = 1
         endif
         if (istopsearch.eq.0) then            
            if (iverbose.gt.0.and.iii.eq.1) then
               call printstatus(1,k,p1,p2,qph,xsec,var,varbefore,sdif,
     .              sdifmax,fmax)
            endif
         endif

!! unweightening for unweighted events...
         if (k.gt.nsearch) then
            istopsearch = 1
            if (hitpmiss.lt.1.d0) then
               fmax = 1.05d0*sdifmax
               print*,'Starting now also unweighted generation!'
c               print*,' --> !!!!!!!NOT RAISED!!!!!!! <-- '
            endif
            hitpmiss = hitpmiss + 1.d0
            call ranlux(csi,1)
            if (fmax*csi(1).lt.sdif) then
               hit = hit + 1.d0
               if (store.eq.'yes') call eventstorage(p1,p2,qph)
            endif
            if (sdif.lt.-1.d-20) then 
               nneg  = nneg  + 1
               sumneg  = sumneg + abs(sdif)
               sum2neg = sum2neg + sdif**2
            endif
            if (sdif.gt.fmax) then 
               nover = nover + 1
               sumover  = sumover  + sdif - fmax
               sum2over = sum2over + (sdif - fmax)**2

               if (iverbose.gt.0) then
                  call printstatus(2,k,p1,p2,qph,xsec,var,varbefore,
     .                 sdif,sdifmax,fmax)
               endif
            endif
         endif

         sum  = sum  + sdif
         sum2 = sum2 + sdif**2

         if (ibranch.eq.1) sumb1 = sumb1 + sdif
         if (ibranch.eq.2) sumb2 = sumb2 + sdif

         sump(ng)  = sump(ng)  + sdif
         sum2p(ng) = sum2p(ng) + sdif**2

         varbefore = var
         xsec = sum/k
         var  = sqrt(abs((sum2/k-xsec**2)/k))
         tollerate = 2.d0
         if (var.gt.tollerate*varbefore
     .        .and.varbefore.gt.0.d0.and.ie.eq.0) then
            if (iverbose.gt.0) then
               call printstatus(3,k,p1,p2,qph,xsec,var,varbefore,sdif,
     .              sdifmax,fmax)
            endif
!---------- Nullifying this event ------------!
            ratio = var/varbefore
c            if (k.gt.5000000.and.ratio.gt.3) then
            if (k.gt.5000000.and.ratio.gt.10) then
               print*,' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
               print*,' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
               print*,' >>>>>>>> REJECTING THE EVENT <<<<<<<<'
               print*,' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
               print*,' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
               if (iverbose.gt.0) then
                  call printstatus(4,k,p1,p2,qph,xsec,var,varbefore,
     .                 sdif,sdifmax,fmax)
               endif
               sum = sum - sdif
               sum2 = sum2 - (sdif)**2
               sump(ng) = sump(ng) - sdif
               sum2p(ng) = sum2p(ng) - (sdif**2)
               xsec = sum/k
               var  = sqrt((sum2/k-xsec**2)/k)
               sdif = 0.d0
            endif
         endif
*
         if (icalc.eq.1.and.ie.eq.0) then
            call distributions(sdif,k,p1,p2,qph)
c$$$            do ii = 0,3
c$$$               ptmp(ii) = p1(ii)+p2(ii)
c$$$            enddo
c$$$            sss = sqrt(dot(ptmp,ptmp))
c$$$            pmod1b = sqrt(tridot(p1b,p1b))
c$$$            pmod2b = sqrt(tridot(p2b,p2b))
c$$$            pmod1  = sqrt(tridot(p1,p1))
c$$$            pmod2  = sqrt(tridot(p2,p2))
c$$$            a1 = acos(p1b(3)/pmod1b)*180.d0/pi
c$$$            a2 = acos(p2b(3)/pmod2b)*180.d0/pi
c$$$            a3 = acos(p1(3)/pmod1)*180.d0/pi
c$$$            a4 = acos(p2(3)/pmod2)*180.d0/pi
c$$$            if (sss.lt.0.5d0.and.a1.lt.20.d0) then
c$$$               print*,'==',nph,sss,qph(1,0)
c$$$               print*,a1,a3
c$$$               print*,a2,a4
c$$$            endif
         endif
*
**[[WRITING OUTPUT]]
         if (mod(k,iwriteout).eq.0.or.k.eq.npoints.or.hit.eq.npoints)
     .        then
            call writedistributions    
!! hit or miss cross section...
            hmxsect = 0.d0
            hmerr   = 0.d0
            if (hitpmiss.gt.0.d0)  then 
               hmeff   = hit/hitpmiss
               hmxsect = fmax*hmeff
               hmerr   = fmax * sqrt(hmeff*(1-hmeff)/hitpmiss)
            endif
!!            
            xsec = sum/k
            var  = sqrt((abs(sum2/k-xsec**2))/k)            
            open(10,file=outfile,status='unknown')            
            write(10,*)' '
            write(10,'(1x,A,A,A)')
     >           'final state = ',fs,' '
            write(10,'(1x,A,f12.4,A)')
     >           'ecms   =',ecms,' GeV'
            write(10,'(1x,A,f12.4,A)')
     >           'thmin  =',thmin*180.d0/pi,' deg'
            write(10,'(1x,A,f12.4,A)')
     >           'thmax  =',thmax*180.d0/pi,' deg'
            write(10,'(1x,A,f12.4,A)')
     >           'acoll. =',zmax*180.d0/pi,' deg'
            write(10,'(1x,A,f12.4,A)')
     >           'emin   = ',emin,' GeV'
            write(10,'(1x,A)')
     >           'ord    = '//ord
            write(10,'(1x,A)')
     >           'model  = '//model
            write(10,'(1x,A,i5)')
     >           'nphot mode =',nphotmode
            write(10,'(1x,A,i9)')
     >           'seed   =',iseed
            write(10,'(1x,A,i5)')
     >           'iarun  =',iarun
            write(10,'(1x,A,f10.9)')
     >           'eps    = ',eps
            write(10,'(1x,A,i5)')
     >           'darkmod =',idarkon
            if(idarkon.eq.1) then
            write(10,'(1x,A,e14.7,A)')
     >           'U mass  = ',amassU, ' GeV'
            write(10,'(1x,A,e14.7,A)')
     >           'U width = ',gammaU, ' GeV'
            write(10,'(1x,A,e14.7,A)')
     >           'k       = ',gvectU/sqrt(4.*pi*alpha), 
     >           ' * electric charge'
            write(10,'(1x,A,f7.3,A)')
     >           'egmin   = ', egmin,  ' GeV'
            write(10,'(1x,A,f7.3,A)')
     >           'thgmin  = ', thgmin*180.d0/pi, ' deg'
            write(10,'(1x,A,f7.3,A)')
     >           'thgmax  = ', thgmax*180.d0/pi, ' deg'
            endif
            write(10,*)' '
            if (eventlimiter.eq.'w') then
               write(10,'(A,f12.0,A)')'~ Generating ',wnpoints,
     .              ' weighted events ~'
            else
               write(10,'(A,i12,A)')'~ Generating ',npoints,
     .              ' unweighted events ~'
            endif
            write(10,*)' '
            write(10,*)'::::::>>>>>> weighted events <<<<<<::::::'
            do i = 0,nphmax
               xsecp(i) = sump(i)/k
               varp(i)  = sqrt((abs(sum2p(i)/k-xsecp(i)**2))/k)
               fractions(i) = xsecp(i)/xsec * 100.d0
               iii = i
               if (fs.eq.'gg') iii = i + 2
               write(10,'(i2,A,f16.8,A,f16.8,A,f8.4,A)')
     :              iii,' photons: ',xsecp(i),' +-',varp(i),
     :              '     (',fractions(i),' %)'
            enddo
            write(10,'(1x,A,f16.8,A,f16.8,A)')
     :           'total:   ',xsec,' +-',var,' nb'
ccc            write(10,*)xsec,' +- ',var
ccc            write(10,*)xsec*1000.d0,' +- ',var*1000.d0
ccc            write(10,*)xsec*100.d0*(1.d0/alpha)**nphotmode,' +- ',
ccc     :           var*100.d0*(1.d0/alpha)**nphotmode
            write(10,*)' '
c            write(10,*)'fractions (%)',(fractions(i),i=0,nphmax)
            write(10,'(1x,A,i12,A,f20.0)')
     :           'n. ',k,' of ',wnpoints
            eff = (1.d0*naccepted)/k
c            write(10,'(1x,A,i12)')
c     :           'accepted ',naccepted
            write(10,'(1x,A,f6.2,A)')
     :           'cut points ',100.d0 - eff*100,' %'
            write(10,*)'::::::>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<::::::'
            write(10,*)' '
            write(10,*)'::::::>>>>>> unweighted events <<<<<<::::::'
            write(10,'(1x,A,f6.2,A)')
     :           'hit or miss efficiency ',hmeff*100,' %'
c            write(10,'(1x,A,f12.0,A,f12.0)')
c     :           'hit+missed and hit points ',hitpmiss,' ',hit
            write(10,'(1x,A,f12.0)')
     :           'unweighted events generated ',hit
            biashit        = 0.d0
            biashitpmiss   = 0.d0
            biasneghit     = 0.d0
            biasneghitmiss = 0.d0
            sezover        = 0.d0
            errsezover     = 0.d0
            if (hit.gt.0.d0) then 
               biashit      = 1.d0*nover/hit
               biashitpmiss = 1.d0*nover/hitpmiss
               biasneghit   = 1.d0*nneg/hit
               biasneghitmiss = 1.d0*nneg/hitpmiss
               sezover    = sumover/hitpmiss
               errsezover = (sum2over/hitpmiss - sezover**2)/hitpmiss
               errsezover = sqrt(abs(errsezover))
               sezneg    = sumneg/hitpmiss
               errsezneg = (sum2neg/hitpmiss - sezneg**2)/hitpmiss
               errsezneg = sqrt(abs(errsezneg))
            endif
            write(10,*)' '
            write(10,'(1x,A,f16.8,A,f16.8,A)')
     :           'total (nb):   ',hmxsect,' +-',hmerr,' +'
            write(10,'(1x,A,f16.8,A,f16.8,A)')
     :           '       !!!!   ',sezover,' +-',
     :           errsezover,' (bias over fmax) +'
            write(10,'(1x,A,f16.8,A,f16.8,A)')
     :           '       !!!!   ',-sezneg,' +-',
     :           errsezneg,' (bias negative)'
            write(10,'(1x,A,f16.8,A,f16.8)')
     :           'total + biases: ',hmxsect
     :           +sezover-sezneg,' +-',hmerr+errsezover+errsezneg
            write(10,*)' '
            write(10,'(1x,A,i12)')
     :           'N. points with w > fmax (bias): ',nover
            write(10,'(1x,A,f10.7,A,f10.7,A)')
     :           'bias/hit and bias/(hit+missed):',
     :           biashit*100,' % and ',biashitpmiss*100,' %'
            write(10,'(1x,A,i12)')
     :           'N. points with w < 0:',nneg
            write(10,'(1x,A,f10.5,A,f10.5,A)')
     :           'biases for w < 0:   ',
     :           biasneghit*100,' % and ',biasneghitmiss*100,' %'
            write(10,*)' '
            write(10,'(1x,A,f14.6,A,f14.6)')
     :           'Upper limits fmax and sdifmax ',fmax,' ',sdifmax
            nwhenprint = nwhenmax
            if (fs.eq.'gg') nwhenprint = nwhenmax+2
            write(10,'(1x,A,i3,A)')
     :           'when there were ',nwhenprint,' photons'
            write(10,*)'::::::>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<::::::'
            write(10,*)' '
            close(10)
         endif
**[[END WRITING OUTPUT]]
         if (eventlimiter.eq.'unw') wnpoints = 1.d13
         if (eventlimiter.eq.'unw'.and.hit.ge.npoints) goto 100
      enddo
 100  continue
      if (store.eq.'yes') call finalizestorage
      print*,'Generation finished'
      stop
      end
